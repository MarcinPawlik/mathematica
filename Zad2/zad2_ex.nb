(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     11760,        277]
NotebookOptionsPosition[     11451,        261]
NotebookOutlinePosition[     11788,        276]
CellTagsIndexPosition[     11745,        273]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"DynamicModule", "[", "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"{", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"a", " ", "=", " ", 
      RowBox[{"Table", "[", 
       RowBox[{
        RowBox[{"RandomInteger", "[", 
         RowBox[{"{", 
          RowBox[{"0", ",", "1"}], "}"}], "]"}], ",", "3", ",", "3"}], 
       "]"}]}], ",", "\[IndentingNewLine]", "g"}], "\[IndentingNewLine]", 
    "}"}], ",", "\[IndentingNewLine]", 
   RowBox[{"Grid", "[", "\[IndentingNewLine]", 
    RowBox[{"{", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"{", "\[IndentingNewLine]", 
       RowBox[{"g", "=", " ", 
        RowBox[{
         RowBox[{"Table", "[", 
          RowBox[{
           RowBox[{"EventHandler", "[", 
            RowBox[{
             RowBox[{"Dynamic", "[", "c3", "]"}], ",", 
             RowBox[{"{", 
              RowBox[{"\"\<MouseClicked\>\"", "\[RuleDelayed]", 
               RowBox[{"(", 
                RowBox[{"c3", "=", 
                 RowBox[{"c3", "/.", 
                  RowBox[{"{", 
                   RowBox[{
                    RowBox[{"1", "\[Rule]", "0"}], ",", 
                    RowBox[{"0", "\[Rule]", "1"}]}], "}"}]}]}], ")"}]}], 
              "}"}]}], "]"}], ",", " ", 
           RowBox[{"{", 
            RowBox[{"i", ",", "3"}], "}"}], ",", " ", 
           RowBox[{"{", 
            RowBox[{"j", ",", "3"}], "}"}]}], "]"}], "//", "MatrixForm"}]}], 
       "\[IndentingNewLine]", "\[IndentingNewLine]", "}"}], ",", 
      "\[IndentingNewLine]", 
      RowBox[{"{", "\[IndentingNewLine]", 
       RowBox[{"Button", "[", 
        RowBox[{"\"\<Save\>\"", ",", 
         RowBox[{"Export", "[", 
          RowBox[{"\"\<zzz.png\>\"", ",", "g"}], "]"}]}], "]"}], 
       "\[IndentingNewLine]", "}"}]}], "\[IndentingNewLine]", "}"}], 
    "\[IndentingNewLine]", "]"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.673542082765771*^9, 3.673542082779065*^9}, {
   3.674194938792591*^9, 3.674195035169009*^9}, {3.674195065384982*^9, 
   3.6741950949352503`*^9}, {3.674195775501775*^9, 3.674195780397389*^9}, {
   3.6741958282545*^9, 3.674196031842795*^9}, {3.674196145484996*^9, 
   3.674196148182383*^9}, {3.674196187655629*^9, 3.674196261519964*^9}, {
   3.674196384891892*^9, 3.6741964342148457`*^9}, {3.674196563741457*^9, 
   3.674196627126502*^9}, {3.6741967403942747`*^9, 3.674196743446307*^9}, {
   3.674196935472828*^9, 3.674196950545767*^9}, {3.674197045906041*^9, 
   3.6741971505998373`*^9}, {3.674197195068982*^9, 3.6741972036642637`*^9}, {
   3.674197261360133*^9, 3.674197327128014*^9}, {3.67419737690569*^9, 
   3.6741973826370687`*^9}, 3.674197504450573*^9, {3.6741976141825047`*^9, 
   3.6741976532042217`*^9}, {3.674197708143444*^9, 3.6741977122126303`*^9}, {
   3.67419834771767*^9, 3.674198348052412*^9}, 3.6741983968086987`*^9, {
   3.6741985522381477`*^9, 3.674198577190914*^9}, 3.6741986188748627`*^9, {
   3.674199096204769*^9, 3.674199253066678*^9}, {3.674199756025552*^9, 
   3.6741997720143147`*^9}, {3.674199833773937*^9, 3.674199834368449*^9}, {
   3.674199888942046*^9, 3.674199903548705*^9}, {3.674200075857335*^9, 
   3.674200078610198*^9}, {3.674200119508459*^9, 3.6742001603437853`*^9}, {
   3.674200206940298*^9, 3.674200235611446*^9}, {3.674200319638262*^9, 
   3.674200325551258*^9}, {3.674200421331599*^9, 3.674200421557528*^9}, {
   3.674200461442968*^9, 3.674200602855797*^9}, {3.674200690823765*^9, 
   3.674200745464191*^9}, {3.6742007958707037`*^9, 3.674200807715444*^9}, {
   3.674212822904808*^9, 3.674212827977265*^9}, {3.6742244997671022`*^9, 
   3.674224502921118*^9}, {3.674225920023828*^9, 3.6742259682024517`*^9}}],

Cell[BoxData[
 DynamicModuleBox[{$CellContext`a1$$ = 1, $CellContext`a2$$ = 
  0, $CellContext`a3$$ = 0, $CellContext`b1$$ = 0, $CellContext`b2$$ = 
  1, $CellContext`b3$$ = 1, $CellContext`c1$$ = 0, $CellContext`c2$$ = 
  1, $CellContext`c3$$ = 1, $CellContext`g$$ = MatrixForm[{{
     EventHandler[
      Dynamic[$CellContext`a1$$], {
      "MouseClicked" :> ($CellContext`a1$$ = 
        ReplaceAll[$CellContext`a1$$, {1 -> 0, 0 -> 1}])}], 
     EventHandler[
      Dynamic[$CellContext`a2$$], {
      "MouseClicked" :> ($CellContext`a2$$ = 
        ReplaceAll[$CellContext`a2$$, {1 -> 0, 0 -> 1}])}], 
     EventHandler[
      Dynamic[$CellContext`a3$$], {
      "MouseClicked" :> ($CellContext`a3$$ = 
        ReplaceAll[$CellContext`a3$$, {1 -> 0, 0 -> 1}])}]}, {
     EventHandler[
      Dynamic[$CellContext`b1$$], {
      "MouseClicked" :> ($CellContext`b1$$ = 
        ReplaceAll[$CellContext`b1$$, {1 -> 0, 0 -> 1}])}], 
     EventHandler[
      Dynamic[$CellContext`b2$$], {
      "MouseClicked" :> ($CellContext`b2$$ = 
        ReplaceAll[$CellContext`b2$$, {1 -> 0, 0 -> 1}])}], 
     EventHandler[
      Dynamic[$CellContext`b3$$], {
      "MouseClicked" :> ($CellContext`b3$$ = 
        ReplaceAll[$CellContext`b3$$, {1 -> 0, 0 -> 1}])}]}, {
     EventHandler[
      Dynamic[$CellContext`c1$$], {
      "MouseClicked" :> ($CellContext`c1$$ = 
        ReplaceAll[$CellContext`c1$$, {1 -> 0, 0 -> 1}])}], 
     EventHandler[
      Dynamic[$CellContext`c2$$], {
      "MouseClicked" :> ($CellContext`c2$$ = 
        ReplaceAll[$CellContext`c2$$, {1 -> 0, 0 -> 1}])}], 
     EventHandler[
      Dynamic[$CellContext`c3$$], {
      "MouseClicked" :> ($CellContext`c3$$ = 
        ReplaceAll[$CellContext`c3$$, {1 -> 0, 0 -> 1}])}]}}]}, 
  TagBox[GridBox[{
     {
      TagBox[
       RowBox[{"(", "\[NoBreak]", GridBox[{
          {
           TagBox[
            DynamicBox[ToBoxes[$CellContext`a1$$, StandardForm],
             ImageSizeCache->{9., {0., 10.}}],
            
            EventHandlerTag[{
             "MouseClicked" :> ($CellContext`a1$$ = 
               ReplaceAll[$CellContext`a1$$, {1 -> 0, 0 -> 1}]), Method -> 
              "Preemptive", PassEventsDown -> Automatic, PassEventsUp -> 
              True}]], 
           TagBox[
            DynamicBox[ToBoxes[$CellContext`a2$$, StandardForm],
             ImageSizeCache->{9., {0., 10.}}],
            
            EventHandlerTag[{
             "MouseClicked" :> ($CellContext`a2$$ = 
               ReplaceAll[$CellContext`a2$$, {1 -> 0, 0 -> 1}]), Method -> 
              "Preemptive", PassEventsDown -> Automatic, PassEventsUp -> 
              True}]], 
           TagBox[
            DynamicBox[ToBoxes[$CellContext`a3$$, StandardForm],
             ImageSizeCache->{9., {0., 10.}}],
            
            EventHandlerTag[{
             "MouseClicked" :> ($CellContext`a3$$ = 
               ReplaceAll[$CellContext`a3$$, {1 -> 0, 0 -> 1}]), Method -> 
              "Preemptive", PassEventsDown -> Automatic, PassEventsUp -> 
              True}]]},
          {
           TagBox[
            DynamicBox[ToBoxes[$CellContext`b1$$, StandardForm],
             ImageSizeCache->{9., {0., 10.}}],
            
            EventHandlerTag[{
             "MouseClicked" :> ($CellContext`b1$$ = 
               ReplaceAll[$CellContext`b1$$, {1 -> 0, 0 -> 1}]), Method -> 
              "Preemptive", PassEventsDown -> Automatic, PassEventsUp -> 
              True}]], 
           TagBox[
            DynamicBox[ToBoxes[$CellContext`b2$$, StandardForm],
             ImageSizeCache->{9., {0., 10.}}],
            
            EventHandlerTag[{
             "MouseClicked" :> ($CellContext`b2$$ = 
               ReplaceAll[$CellContext`b2$$, {1 -> 0, 0 -> 1}]), Method -> 
              "Preemptive", PassEventsDown -> Automatic, PassEventsUp -> 
              True}]], 
           TagBox[
            DynamicBox[ToBoxes[$CellContext`b3$$, StandardForm],
             ImageSizeCache->{9., {0., 10.}}],
            
            EventHandlerTag[{
             "MouseClicked" :> ($CellContext`b3$$ = 
               ReplaceAll[$CellContext`b3$$, {1 -> 0, 0 -> 1}]), Method -> 
              "Preemptive", PassEventsDown -> Automatic, PassEventsUp -> 
              True}]]},
          {
           TagBox[
            DynamicBox[ToBoxes[$CellContext`c1$$, StandardForm],
             ImageSizeCache->{9., {0., 10.}}],
            
            EventHandlerTag[{
             "MouseClicked" :> ($CellContext`c1$$ = 
               ReplaceAll[$CellContext`c1$$, {1 -> 0, 0 -> 1}]), Method -> 
              "Preemptive", PassEventsDown -> Automatic, PassEventsUp -> 
              True}]], 
           TagBox[
            DynamicBox[ToBoxes[$CellContext`c2$$, StandardForm],
             ImageSizeCache->{9., {0., 10.}}],
            
            EventHandlerTag[{
             "MouseClicked" :> ($CellContext`c2$$ = 
               ReplaceAll[$CellContext`c2$$, {1 -> 0, 0 -> 1}]), Method -> 
              "Preemptive", PassEventsDown -> Automatic, PassEventsUp -> 
              True}]], 
           TagBox[
            DynamicBox[ToBoxes[$CellContext`c3$$, StandardForm],
             ImageSizeCache->{9., {0., 10.}}],
            
            EventHandlerTag[{
             "MouseClicked" :> ($CellContext`c3$$ = 
               ReplaceAll[$CellContext`c3$$, {1 -> 0, 0 -> 1}]), Method -> 
              "Preemptive", PassEventsDown -> Automatic, PassEventsUp -> 
              True}]]}
         },
         GridBoxAlignment->{
          "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, 
           "Rows" -> {{Baseline}}, "RowsIndexed" -> {}},
         GridBoxSpacings->{"Columns" -> {
             Offset[0.27999999999999997`], {
              Offset[0.7]}, 
             Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
             Offset[0.2], {
              Offset[0.4]}, 
             Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
       Function[BoxForm`e$, 
        MatrixForm[BoxForm`e$]]]},
     {
      ButtonBox["\<\"Save\"\>",
       Appearance->Automatic,
       ButtonFunction:>Export["zzz.png", $CellContext`g$$],
       Evaluator->Automatic,
       Method->"Preemptive"]}
    },
    AutoDelete->False,
    GridBoxItemSize->{"Columns" -> {{Automatic}}, "Rows" -> {{Automatic}}}],
   "Grid"],
  DynamicModuleValues:>{}]], "Output",
 CellChangeTimes->{{3.6741971981969223`*^9, 3.674197204403782*^9}, {
   3.6741972749394484`*^9, 3.674197329158046*^9}, {3.67419738954171*^9, 
   3.67419740205733*^9}, 3.6741974350029497`*^9, 3.6741975064552383`*^9, {
   3.6741983183196383`*^9, 3.674198327524564*^9}, {3.674198596025717*^9, 
   3.67419862257191*^9}, {3.6741990998097687`*^9, 3.674199122120556*^9}, {
   3.674199170345434*^9, 3.674199209621161*^9}, {3.674199242612137*^9, 
   3.67419925431771*^9}, {3.674199761858121*^9, 3.674199772458682*^9}, 
   3.67419983841175*^9, {3.674199896895859*^9, 3.674199906918537*^9}, 
   3.674200080258059*^9, 3.6742004227420273`*^9, {3.674200464228948*^9, 
   3.674200572513369*^9}, 3.674200621836877*^9, {3.6742007082589703`*^9, 
   3.674200746656299*^9}, {3.67420079787989*^9, 3.674200816668419*^9}, 
   3.674214538890102*^9, 3.6742245045953083`*^9}]
}, Open  ]]
},
WindowSize->{1366, 721},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"10.3 for Linux x86 (64-bit) (December 10, 2015)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 3635, 70, 363, "Input"],
Cell[4218, 94, 7217, 164, 91, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
